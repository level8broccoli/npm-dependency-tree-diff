{
  inputs = {
    utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
    in
    {
      devShell = pkgs.mkShell {
        buildInputs = with pkgs; [
          cargo
          rustfmt
          rust-analyzer
          rustc
          clippy
        ];
        RUST_BACKTRACE = 1;
      };
    }
  );
}
