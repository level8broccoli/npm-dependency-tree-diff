# npm-dependency-tree-diff

Made for outputs generated by running in two different npm projects: 
```sh
$ npm ls --all --json
```

## Usage

Generate a diff of two different dependency trees by running:
```sh
$ cargo run -- [path to old dependency tree (*.json)] [path to new dependency tree (*.json)]

# e.g.
$ cargo run -- ./json-examples/single-dependency-altered-version.json ./json-examples/nested-dependencies.json
```

## Options

The four different change types are `UNCHANGED`, `CHANGED`, `REMOVED`, `ADDED`. All those are being outputted by default. They can be filtered out, but only the nodes which have no child nodes or all child nodes have also been filtered out. (See Example 2)

```sh
-u, --unchanged  Filter out all UNCHANGED leafs of the diff tree
-r, --removed    Filter out all REMOVED leafs of the diff tree
-a, --added      Filter out all ADDED leafs of the diff tree
-c, --changed    Filter out all CHANGED leafs of the diff tree
-h, --help       Print help
```

## Examples

### Example 1

- Input 1

```json
{
  "version": "1.0.0",
  "name": "generate-npm-deps-output",
  "dependencies": {
    "to-no-case": {
      "version": "1.0.2",
      "resolved": "https://registry.npmjs.org/to-no-case/-/to-no-case-1.0.2.tgz",
      "overridden": false
    },
    "typescript": {
      "version": "5.4.5",
      "resolved": "https://registry.npmjs.org/typescript/-/typescript-5.4.5.tgz",
      "overridden": false
    }
  }
} 
```

- Input 2

```json
{
  "version": "1.0.0",
  "name": "generate-npm-deps-output",
  "dependencies": {
    "to-no-case": {
      "version": "1.0.1",
      "resolved": "https://registry.npmjs.org/to-no-case/-/to-no-case-1.0.1.tgz",
      "overridden": false
    },
    "to-snake-case": {
      "version": "1.0.0",
      "resolved": "https://registry.npmjs.org/to-snake-case/-/to-snake-case-1.0.0.tgz",
      "overridden": false,
      "dependencies": {
        "to-space-case": {
          "version": "1.0.0",
          "resolved": "https://registry.npmjs.org/to-space-case/-/to-space-case-1.0.0.tgz",
          "overridden": false,
          "dependencies": {
            "to-no-case": {
              "version": "1.0.1"
            }
          }
        }
      }
    },
    "typescript": {
      "version": "5.4.5",
      "resolved": "https://registry.npmjs.org/typescript/-/typescript-5.4.5.tgz",
      "overridden": false
    }
  }
}
```

- Command

```sh
$ cargo run -- [Input 1] [Input 2]
```

- Output (`./out/diff.json`)


```json
{
  "to-no-case": {
    "change": {
      "ADDED": {
        "new_version": "1.0.1"
      }
    },
    "dependencies": {}
  },
  "to-snake-case": {
    "change": {
      "ADDED": {
        "new_version": "1.0.0"
      }
    },
    "dependencies": {
      "to-space-case": {
        "change": {
          "ADDED": {
            "new_version": "1.0.0"
          }
        },
        "dependencies": {
          "to-no-case": {
            "change": {
              "ADDED": {
                "new_version": "1.0.1"
              }
            },
            "dependencies": {}
          }
        }
      }
    }
  },
  "typescript": {
    "change": {
      "CHANGED": {
        "old_version": "5.4.4",
        "new_version": "5.4.5"
      }
    },
    "dependencies": {}
  }
}
```

### Example 2

- Input 1

```json
{
  "version": "1.0.0",
  "name": "generate-npm-deps-output",
  "dependencies": {
    "to-no-case": {
      "version": "1.0.1",
      "resolved": "https://registry.npmjs.org/to-no-case/-/to-no-case-1.0.1.tgz",
      "overridden": false
    },
    "to-snake-case": {
      "version": "1.0.0",
      "resolved": "https://registry.npmjs.org/to-snake-case/-/to-snake-case-1.0.0.tgz",
      "overridden": false,
      "dependencies": {
        "to-space-case": {
          "version": "1.0.0",
          "resolved": "https://registry.npmjs.org/to-space-case/-/to-space-case-1.0.0.tgz",
          "overridden": false,
          "dependencies": {
            "to-no-case": {
              "version": "1.0.1"
            }
          }
        }
      }
    },
    "typescript": {
      "version": "5.4.5",
      "resolved": "https://registry.npmjs.org/typescript/-/typescript-5.4.5.tgz",
      "overridden": false
    }
  }
}
```

- Input 2

```json
{
  "version": "1.0.0",
  "name": "generate-npm-deps-output",
  "dependencies": {
    "to-snake-case": {
      "version": "1.0.0",
      "resolved": "https://registry.npmjs.org/to-snake-case/-/to-snake-case-1.0.0.tgz",
      "overridden": false,
      "dependencies": {
        "to-space-case": {
          "version": "1.1.0",
          "resolved": "https://registry.npmjs.org/to-space-case/-/to-space-case-1.0.0.tgz",
          "overridden": false,
          "dependencies": {
            "to-no-case": {
              "version": "1.0.0"
            }
          }
        }
      }
    },
    "typescript": {
      "version": "5.4.5",
      "resolved": "https://registry.npmjs.org/typescript/-/typescript-5.4.5.tgz",
      "overridden": false
    }
  }
}
```

- Command

```sh
$ cargo run -- [Input 1] [Input 2] --unchanged
```

- Output (`./out/diff.json`)


```json
{
  "to-snake-case": {
    "change": {
      "UNCHANGED": {
        "version": "1.0.0"
      }
    },
    "dependencies": {
      "to-space-case": {
        "change": {
          "CHANGED": {
            "old_version": "1.0.0",
            "new_version": "1.1.0"
          }
        },
        "dependencies": {
          "to-no-case": {
            "change": {
              "CHANGED": {
                "old_version": "1.0.1",
                "new_version": "1.0.0"
              }
            },
            "dependencies": {}
          }
        }
      }
    }
  },
  "to-no-case": {
    "change": {
      "REMOVED": {
        "old_version": "1.0.1"
      }
    },
    "dependencies": {}
  }
}
```
