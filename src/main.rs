use anyhow::Result;
use clap::Parser;
use core::panic;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::fs;
use std::path::PathBuf;

/// An diff tool for comparing the dependencies tree of two npm projects.
#[derive(Parser)]
struct Cli {
    /// Path to a file which includes the dependency tree of a npm project.
    file_path_before: PathBuf,
    /// Path to a file which includes the dependency tree of a npm project.
    file_path_after: PathBuf,
    /// Filter out all UNCHANGED leafs of the diff tree
    #[arg(short, long, default_value_t = false)]
    unchanged: bool,
    /// Filter out all REMOVED leafs of the diff tree
    #[arg(short, long, default_value_t = false)]
    removed: bool,
    /// Filter out all ADDED leafs of the diff tree
    #[arg(short, long, default_value_t = false)]
    added: bool,
    /// Filter out all CHANGED leafs of the diff tree
    #[arg(short, long, default_value_t = false)]
    changed: bool,
}

#[derive(Deserialize, Debug, Clone)]
struct Dependency {
    version: String,
    dependencies: Option<HashMap<String, Dependency>>,
}

#[derive(Serialize, Debug)]
enum VersionChange {
    UNCHANGED {
        version: String,
    },
    REMOVED {
        old_version: String,
    },
    ADDED {
        new_version: String,
    },
    CHANGED {
        old_version: String,
        new_version: String,
    },
}

#[derive(Serialize, Debug)]
struct DependencyDiff {
    change: VersionChange,
    dependencies: HashMap<String, DependencyDiff>,
}

fn parse_file(path: &PathBuf) -> Result<Dependency> {
    let content = fs::read_to_string(path)?;
    let parsed = serde_json::from_str(&content)?;
    Ok(parsed)
}

fn compute_diff(
    old_dependencies: Option<HashMap<String, Dependency>>,
    new_dependencies: Option<HashMap<String, Dependency>>,
) -> HashMap<String, DependencyDiff> {
    let key_set = match (&old_dependencies, &new_dependencies) {
        (None, None) => HashSet::new(),
        (Some(o), None) => o.keys().cloned().collect(),
        (None, Some(n)) => n.keys().cloned().collect(),
        (Some(o), Some(n)) => o.keys().chain(n.keys()).cloned().collect(),
    };

    let mut diff = HashMap::new();

    for key in key_set {
        let old_value = old_dependencies
            .as_ref()
            .and_then(|od| od.get(&key))
            .cloned();
        let new_value = new_dependencies
            .as_ref()
            .and_then(|nd| nd.get(&key))
            .cloned();

        match (old_value, new_value) {
            (None, None) => panic!("This should never happen"),
            (Some(o), None) => diff.insert(
                key,
                DependencyDiff {
                    change: VersionChange::REMOVED {
                        old_version: o.version,
                    },
                    dependencies: compute_diff(o.dependencies.clone(), None),
                },
            ),
            (None, Some(n)) => diff.insert(
                key,
                DependencyDiff {
                    change: VersionChange::ADDED {
                        new_version: n.version,
                    },
                    dependencies: compute_diff(None, n.dependencies.clone()),
                },
            ),
            (Some(o), Some(n)) => {
                if o.version == n.version {
                    diff.insert(
                        key,
                        DependencyDiff {
                            change: VersionChange::UNCHANGED { version: o.version },
                            dependencies: compute_diff(
                                o.dependencies.clone(),
                                n.dependencies.clone(),
                            ),
                        },
                    )
                } else {
                    diff.insert(
                        key,
                        DependencyDiff {
                            change: VersionChange::CHANGED {
                                old_version: o.version,
                                new_version: n.version,
                            },
                            dependencies: compute_diff(
                                o.dependencies.clone(),
                                n.dependencies.clone(),
                            ),
                        },
                    )
                }
            }
        };
    }

    diff
}

fn filter_diff(
    diff: HashMap<String, DependencyDiff>,
    unchanged: bool,
    changed: bool,
    added: bool,
    removed: bool,
) -> HashMap<String, DependencyDiff> {
    let mut new_diff = HashMap::new();
    for (key, value) in diff {
        let dependencies = filter_diff(value.dependencies, unchanged, changed, added, removed);

        match (dependencies.len(), value.change) {
            (0, VersionChange::UNCHANGED { version: _ }) if unchanged => None,
            (
                0,
                VersionChange::CHANGED {
                    old_version: _,
                    new_version: _,
                },
            ) if changed => None,
            (0, VersionChange::REMOVED { old_version: _ }) if removed => None,
            (0, VersionChange::ADDED { new_version: _ }) if added => None,
            (_, VersionChange::UNCHANGED { version }) => new_diff.insert(
                key,
                DependencyDiff {
                    change: VersionChange::UNCHANGED { version },
                    dependencies,
                },
            ),
            (
                _,
                VersionChange::CHANGED {
                    old_version,
                    new_version,
                },
            ) => new_diff.insert(
                key,
                DependencyDiff {
                    change: VersionChange::CHANGED {
                        old_version,
                        new_version,
                    },
                    dependencies,
                },
            ),
            (_, VersionChange::ADDED { new_version }) => new_diff.insert(
                key,
                DependencyDiff {
                    change: VersionChange::ADDED { new_version },
                    dependencies,
                },
            ),
            (_, VersionChange::REMOVED { old_version }) => new_diff.insert(
                key,
                DependencyDiff {
                    change: VersionChange::REMOVED { old_version },
                    dependencies,
                },
            ),
        };
    }
    new_diff
}

fn main() -> Result<()> {
    let args = Cli::parse();

    let old_dependencies = parse_file(&args.file_path_before)?.dependencies;
    let new_dependencies = parse_file(&args.file_path_after)?.dependencies;

    let diff = compute_diff(old_dependencies, new_dependencies);

    let filtered = filter_diff(diff, args.unchanged, args.changed, args.added, args.removed);

    let json = serde_json::to_string(&filtered)?;
    fs::create_dir_all("./out")?;
    fs::write("./out/diff.json", json)?;

    Ok(())
}
